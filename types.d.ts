declare global {
  namespace NodeJS {
    interface Global {
      prisma: unknown;
    }
  }
}

declare module "uppy-plugin-image-compressor";
