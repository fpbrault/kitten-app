/* eslint-disable @typescript-eslint/no-var-requires */
const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");
const nextTranslate = require("next-translate");
const { withSuperjson } = require('next-superjson')

const config = {
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: false
  },
  publicRuntimeConfig: {
    NEXTAUTH_URL: process.env.VERCEL_URL
      ? "https://" + process.env.VERCEL_URL
      : process.env.NEXTAUTH_URL
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    DATABASE_URL: process.env.DATABASE_URL,
    SECRET: process.env.SECRET,
    JWT_SECRET: process.env.JWT_SECRET,
    JWT_SIGNING_KEY: process.env.JWT_SIGNING_KEY,
    JWT_ENCRYPTION_KEY: process.env.JWT_ENCRYPTION_KEY
  },
  i18n: {
    // These are all the locales you want to support in
    // your application
    locales: ["en", "fr"],
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: "en"
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'kitten-app-storage.s3.amazonaws.com',
        pathname: '/**',
      }, {
        protocol: 'https',
        hostname: 'lh3.googleusercontent.com',
        pathname: '/**',
      },
    ],
    loaderFile: './lib/imageLoader.js',
  },
  webpack: (config) => {
    return config;
  }
}

const pwaConfig = {
  pwa: {
    dest: "public",
    runtimeCaching
  },
  ...config
}


if (process.env.DISABLE_PWA == "TRUE") {
  module.exports = withSuperjson()(
    nextTranslate(config));
} else {
  module.exports = withSuperjson()(nextTranslate(
    withPWA(pwaConfig))
  );
}
