import { Prisma } from '@prisma/client'

export type KittenProps = {
  id: number;
  name: string;
  posts: KittenPostProps[];
  litter: {
    name: string;
    id: number;
  };
  datapoints: Prisma.KittenDataPoint[];
  litterId: number;
  content: string;
  image: string;
  birthdate: Date;
};


export type KittenPostProps = {
  id: number;
  title: string;
  content: string;
  createdAt: Date;
  published: boolean;
  image: string;
};

export type KittenDataPostProps = {
  id: number;
  time: Date;
  startWeight: number;
  finalWeight: number;
  kittenId: number;
};

export type PostProps = {
  slug: string;
  source: any;
  frontMatter: {
    title: string,
    excerpt: string,
    coverImage: string,
    kitten: string,
    date: string,
    author: {
      name: string,
      picture: string
    },
    ogImage: {
      url: string
    };
  };
}

export type LitterProps = {
  id: number;
  name: string;
  description: string;
  Kitten: {
    id: number;
    name: string;
    litter: {
      name: string;
      id: number;
    } | null;
    litterId: number;
    content: string;
    posts: Array<string>;
    image: string;
  };
};