import React from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { PostProps } from "types/kittens";



const Post: React.FC<{ post: PostProps }> = ({ post }) => {
  const { t } = useTranslation("common");
  if (post.frontMatter) {


    return (
      <div className=" lg:max-w-2xl max-w-sm md:max-w-lg mb-8 mx-auto text-center rounded-md shadow-2xl lg:max-h-[400px] lg:even:flex-row-reverse lg:odd:flex-row card lg:card-side bg-base-100 hover:odd:bg-primary hover:even:bg-secondary transition-all">
        {post.frontMatter?.coverImage ? (
          <Link href={"/blog/" + post.slug} className="min-h-full p-4 lg:w-1/2">

            <Image
              className="object-cover hover:outline outline-4 outline-white  min-h-full w-full max-h-full rounded-xl max-w-full first-letter:md"
              height={500}
              width={500}
              src={post.frontMatter?.coverImage}
              alt={post?.frontMatter?.title}
              priority
              sizes="(max-width: 500px) 100vw,
                (max-width: 800 px) 50vw,
                33vw"
            />

          </Link>

        ) : null}
        <div className=" lg:w-1/2 card-body">
          <Link href={"/blog/" + post.slug} className="link link-hover pb-2 lg:text-3xl text-xl font-bold border-b-2 hover:text-base-100">
            {post?.frontMatter?.title}
          </Link>
          <p className="pb-2 text-sm lg:text-base text-grey-darker">
            {post.frontMatter.excerpt.length > 120 ? post.frontMatter.excerpt.slice(0, 120) + "..." : post.frontMatter.excerpt}
          </p>
          <Link href={"/blog/" + post.slug} className="px-1 mx-auto mt-2 text-center text-gray-500 transition-colors bg-transparent border border-gray-400 rounded cursor-pointer focus:outline-none hover:border-transparent hover:bg-accent hover:text-white">

            {t("home.read-more")}

          </Link>
        </div>
      </div>
    );
  } return <></>
};

export default Post;
