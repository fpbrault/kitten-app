/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
import React from "react";
import Link from "next/link";
import { signOut, useSession } from "next-auth/react";
import useTranslation from "next-translate/useTranslation";
//import { useRouter } from "next/router";
import LanguageSwitcher from "components/Generic/LanguageSwitcher";
import { KittenIcon } from "./KittenIcon";

const Header: React.FC = () => {
  //const router = useRouter();
  const { t } = useTranslation("common");
  //const [, setLanguage] = useState(router.locale);
  const { data: session } = useSession();


  const defaultPages = [
    [t("navbar.about"), '/about'],
    ['Blog', '/blog'],
    [t("navbar.our-kittens"), '/kittens'],
  ]
  /*   const adminPages = [
      [t("navbar.about"), '/about'],
      ['Blog', '/blog'],
      [t("navbar.our-kittens"), '/prokittensects'],
    ] */

  const userInfo = session ? (<>
    <div className="text-xs ">
      {session?.user.email}</div>
    {session.user.role === "admin" ? (
      <div className="dropdown dropdown-end">
        <div tabIndex={0} className="btn btn-sm btn-ghost rounded-btn">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-5 h-5"
            viewBox="0 0 20 20"
            fill="currentColor">
            <path
              fillRule="evenodd"
              d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
              clipRule="evenodd"
            />
          </svg>
        </div>
        <ul
          tabIndex={0}
          className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-52">
          <li>
            <Link href="/create">
              {t("navbar.new-post")}
            </Link>
          </li>
          <li>
            <Link href="/data">
              {t("navbar.new-data")}
            </Link>
          </li>
        </ul>
      </div>
    ) : null}
    <a
      className="btn btn-ghost btn-sm rounded-btn"
      role="link"
      tabIndex={0}
      onClick={() => signOut()}
      onKeyPress={() => signOut()}>
      <div className="">{t("navbar.logout")}</div>
    </a>
  </>) : null

  let navMenu = (
    <div className="flex-col lg:flex lg:flex-row">
      {defaultPages.map(([title, url]) => url !== "/kittens" ? (
        <Link key={url} href={url}>
          <button className="btn btn-ghost btn-sm rounded-btn">{title}</button>
        </Link>
      ) : ((<Link key={url} href="/kittens" className="btn btn-ghost btn-sm rounded-btn">
        {KittenIcon}
        {t("navbar.our-kittens")}{" "}
        {KittenIcon}
      </Link>)))}

    </div>
  );

  let rightMenu = (
    <div className="flex flex-row items-center gap-2 align-middle">
      {userInfo}
      <LanguageSwitcher></LanguageSwitcher>
    </div>
  );


  let compactMenu = (
    <>
      {defaultPages.map(([title, url]) => (
        <li key={url + title}><Link href={url}>
          {title}
        </Link></li>
      ))}
      <li>
        <LanguageSwitcher></LanguageSwitcher>
      </li>
      <li>{userInfo}</li>
    </>
  );


  return (
    <header className="fixed z-20 w-full shadow-lg navbar bg-primary text-base-content">
      <div className="px-2 mx-2 navbar-start">
        <div className="flex-1">
          <Link href="/" className="text-sm font-bold sm:text-lg link link-hover link-neutral">
            Les Petits Chatons
          </Link>
        </div>
      </div>

      <nav className="hidden px-2 mx-2 navbar-center lg:flex">{navMenu}</nav>
      <div className="navbar-end">
        <div className="hidden px-2 mx-2 lg:flex">{rightMenu}</div>
        <div className="flex-none lg:hidden">
          <div className="dropdown dropdown-end">
            <button id="nav" role={"menu"} className="btn btn-square btn-ghost">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-6 h-6 stroke-current">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
            </button>
            <div className="w-64 dropdown-content bg-base-200 rounded-box">
              <ul className="shadow-lg text-neutral menu bg-base-100 rounded-box">{compactMenu}</ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
