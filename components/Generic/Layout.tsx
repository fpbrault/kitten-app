import React, { ReactNode } from "react";
import Header from "components/Generic/Header";
import Head from "next/head";
import { Comfortaa } from '@next/font/google'
import Footer from "components/Generic/Footer";

const comfortaa = Comfortaa({
  subsets: ['latin'],
  variable: '--font-comfortaa'
})

type Props = {
  children: ReactNode;
  pageTitle: string;
  fullpage?: boolean;
};

const Layout: React.FC<Props> = (props) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="Les Petits Chatons" />
        <meta name="keywords" content="Keywords" />
        <title>{props.pageTitle}</title>

        <meta property="og:url" content="https://lespetitschatons.org/" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={props.pageTitle} key="ogtitle" />
        <meta property="og:description" content="Les Petits Chatons" key="ogdesc" />
        <meta property="og:image" content="https://lespetitschatons.org/cat.png" />
        <meta property="twitter:domain" content="lespetitschatons.org" />
        <meta property="twitter:url" content="https://lespetitschatons.org/" />
        <meta name="twitter:image" content="https://lespetitschatons.org/cat.png" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={props.pageTitle} />
        <meta name="twitter:description" content="Les Petits Chatons" />

        <link rel="manifest" href="/manifest.json" />
        <link href="/favicon-16x16.png" rel="icon" type="image/png" sizes="16x16" />
        <link href="/favicon-32x32.png" rel="icon" type="image/png" sizes="32x32" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />

        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="apple-touch-icon" href="/apple-icon.png"></link>
        <meta name="theme-color" content="#317EFB" />

      </Head>

      <div data-theme="kitten" className={`${comfortaa.variable} font-sans flex flex-col text-neutral bg-base-200`}>
        {!props.fullpage ? <Header /> : null}
        <main className="min-h-screen pt-0">{props.children}</main>

        {!props.fullpage ? (
          <Footer />
        ) : null}
      </div>
    </>
  );
};
export default Layout;
