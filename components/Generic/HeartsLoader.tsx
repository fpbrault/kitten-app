import React from "react";
import { Hearts } from "react-loader-spinner";

const HeartsLoader: React.FC = () => (
  <div className="flex justify-center">
    <Hearts color="#00BFFF" height={50} width={50} />
  </div>
);

export default HeartsLoader;
