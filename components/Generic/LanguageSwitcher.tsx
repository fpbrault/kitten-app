import React, { useState } from "react";
import { useRouter } from "next/router";



export default function LanguageSwitcher() {
    const router = useRouter();
    const [language, setLanguage] = useState(router.locale);

    const onChangeLanguage =
        (lang: string) => (e: { preventDefault: () => void }) => {
            e.preventDefault();
            setLanguage(lang);
            router.push(router.asPath, undefined, { locale: lang });
        };

    return (
        <>
            {language === "fr" ? (

                <button
                    onClick={onChangeLanguage("en")}
                    className="lg:gap-2 lg:btn lg:btn-ghost"
                >
                    <svg viewBox="0 0 24 24" className="w-4 h-4">
                        <path
                            fill="currentColor"
                            d="M17.9,17.39C17.64,16.59 16.89,16 16,16H15V13A1,1 0 0,0 14,12H8V10H10A1,1 0 0,0 11,9V7H13A2,2 0 0,0 15,5V4.59C17.93,5.77 20,8.64 20,12C20,14.08 19.2,15.97 17.9,17.39M11,19.93C7.05,19.44 4,16.08 4,12C4,11.38 4.08,10.78 4.21,10.21L9,15V16A2,2 0 0,0 11,18M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"
                        />
                    </svg>
                    EN
                </button>
            ) : (
                <button
                    onClick={onChangeLanguage("fr")}
                    className="lg:gap-2 lg:btn lg:btn-ghost"
                >
                    <svg viewBox="0 0 24 24" className="w-4 h-4">
                        <path
                            fill="currentColor"
                            d="M17.9,17.39C17.64,16.59 16.89,16 16,16H15V13A1,1 0 0,0 14,12H8V10H10A1,1 0 0,0 11,9V7H13A2,2 0 0,0 15,5V4.59C17.93,5.77 20,8.64 20,12C20,14.08 19.2,15.97 17.9,17.39M11,19.93C7.05,19.44 4,16.08 4,12C4,11.38 4.08,10.78 4.21,10.21L9,15V16A2,2 0 0,0 11,18M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"
                        />
                    </svg>
                    FR
                </button>
            )}
        </>
    );
}
