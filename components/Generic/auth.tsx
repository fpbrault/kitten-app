import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { useEffect } from "react";
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Auth = ({ children }: any) => {
  const { data: session, status } = useSession();
  const hasUser = !!session?.user;
  const router = useRouter();
  useEffect(() => {
    if (status == "authenticated" && !hasUser) {
      router.push("/login");
    }
  }, [status, hasUser, router]);
  if (status === "unauthenticated" || !hasUser) {
    return <div>Waiting for session...</div>;
  }
  return children;
};
export default Auth;
