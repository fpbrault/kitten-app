import React from "react";
import Router from "next/router";
import ReactMarkdown from "react-markdown";
import { LitterProps } from "types/kittens";



const Litter: React.FC<{ litter: LitterProps }> = ({ litter }) => {
  return (
    <div
      role="button"
      tabIndex={0}
      onKeyUp={() => Router.push("/litter/[id]", `/litter/${litter.id}`)}
      onClick={() => Router.push("/litter/[id]", `/litter/${litter.id}`)}>
      <h2>{litter.name}</h2>
      <ReactMarkdown>{litter.description}</ReactMarkdown>
    </div>
  );
};

export default Litter;
