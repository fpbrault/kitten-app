/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { AuthorIcon } from "components/Generic/AuthorIcon";
import { PostProps } from "types/kittens";



const MiniPost: React.FC<{ post: PostProps }> = ({ post }) => {
  const { t } = useTranslation("common");
  if (post.frontMatter) {
    const authorName = post.frontMatter.author ? post.frontMatter.author.name.split(" ")[0] : "Les Petits Chatons";
    let day = new Date(post.frontMatter.date).toDateString();
    return (
      <div>

        <div className="flex flex-col lg:max-w-md max-w-xs  mb-4 text-center border-b-2 shadow card lg:shadow-2xl  lg:card bg-base-100 lg:bg-base-100">
          {post.frontMatter?.coverImage ? (
            <Link href={"/blog/" + post.slug} className="min-h-full p-4">


              <Image
                className="object-cover hover:outline outline-4 outline-white  min-h-full w-full max-h-full rounded-xl max-w-full first-letter:md"
                height={500}
                width={500}
                src={post?.frontMatter?.coverImage}
                alt={post?.frontMatter?.title}
                priority
              />

            </Link>
          ) : null}
          <div className="card-body">
            <Link className="mb-2 text-4xl font-bold hover:text-secondary" href={"/blog/" + post.slug}>
              {post?.frontMatter?.title}
            </Link>
            <div className="mx-auto mb-2 text-lg font-thin">
              <div className="flex gap-2 ">

                <AuthorIcon />
                {authorName || "Les Petits Chatons"}
              </div>
            </div>
            <span className="inline-block py-1 text-sm font-semibold rounded-full bg-grey-lighter text-grey-darker">
              {day}
            </span>
            <p className="text-base text-grey-darker">
              {post.frontMatter.excerpt}
            </p>
            <Link href={"/blog/" + post.slug} className="px-4 mx-auto mt-4 text-center text-gray-500 transition-colors bg-transparent border border-gray-400 rounded cursor-pointer h-7 focus:outline-none hover:border-transparent hover:bg-secondary hover:text-white">
              {t("home.read-more")}
            </Link>
          </div>
        </div>
      </div>
    );
  } return <><pre>{JSON.stringify(post, null, 2)}</pre></>
};

export default MiniPost;
