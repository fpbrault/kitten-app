import React, { useState } from 'react'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Select from "react-select";
import NumberPicker from "react-widgets/NumberPicker";
import ImageUpload from "components/Uploader";
import HeartsLoader from "components/Generic/HeartsLoader";
import KittenDataTable from "components/Kittens/KittenDataTable";
import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import type { } from '@mui/x-date-pickers/themeAugmentation';
import { useRouter } from 'next/router'
import type { Litter, Kitten, KittenDataPoint } from '@prisma/client'




const DateTimePicker = dynamic(() => import('@mui/x-date-pickers').then((mod) => mod.DateTimePicker), {
    suspense: false,
})

const TextField = dynamic(() => import('@mui/material').then((mod) => mod.TextField), {
    suspense: false,
})

interface KittenData extends Kitten {
    datapoints: KittenDataPoint[]
}

type Props = {
    data: { litters: Litter[], kittens: KittenData[] },
    refreshData: () => void,
}
type TypeOptions = {
    value: string,
    label: string
}

export default function DataEntryForm({ data, refreshData }: Props) {
    const router = useRouter()

    const typeOptions = [
        { value: "data", label: "Data Point" },
        { value: "litter", label: "Litter" },
        { value: "kitten", label: "Kitten" }
    ];

    const KittenOptions =
        typeof data != "undefined" && typeof data.kittens != "undefined"
            ? data.kittens.map((x: Kitten) => ({ value: x.id.toString(), label: x.name }))
            : [];

    const LitterOptions =
        typeof data != "undefined" && typeof data.litters != "undefined"
            ? data.litters.map((x: Litter) => ({ value: x.id.toString(), label: x.name }))
            : [];

    const [startWeight, setStartWeight] = useState<number | null>(350);
    const [finalWeight, setFinalWeight] = useState<number | null>(350);
    const [selectedTime, setSelectedTime] = useState<Date | null>(new Date());
    const [selectedKitten, setSelectedKitten] = useState<{ value: string, label: string } | null>(null);
    const [kittenName, setKittenName] = useState("");
    const [image, setImage] = useState("");
    const [kittenLitter, setKittenLitter] = useState(null);
    const [kittenBirthday, setKittenBirthday] = useState(new Date());
    const [kittenDescription, setKittenDescription] = useState("");
    const [litterName, setLitterName] = useState("");
    const [litterDescription, setLitterDescription] = useState("");
    let [uploaderVisible, setUploaderVisible] = useState(false);
    const [type, setType] = useState<TypeOptions | null>(typeOptions[0]);
    let [loading, setLoading] = useState(false);



    const changeImageUrl = (url: React.SetStateAction<string>) => {
        setImage(url);
        setUploaderVisible(false);
    };
    const resetInput = () => {
        setImage("");
        setStartWeight(350);
        setFinalWeight(350);
        setSelectedTime(new Date());
        setSelectedKitten(null);
        setKittenName("");
        setKittenLitter(null);
        setKittenBirthday(new Date());
        setKittenDescription("");
        setLitterName("");
        setLitterDescription("");
        setUploaderVisible(false);
    };

    const submitData = async (e: { preventDefault: () => void; }) => {
        e.preventDefault();

        setLoading(true);
        if (type?.value === "data") {
            const body = { startWeight, finalWeight, selectedKitten, selectedTime };
            await fetch(`/api/kittendatapost`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body)
            })
                .then(() => {
                    refreshData();
                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                })
                .catch((error) => {
                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                    console.error(error);
                    refreshData();
                });
        } else if (type?.value === "kitten") {
            const body = { kittenName, kittenDescription, kittenLitter, image, kittenBirthday };
            await fetch(`/api/kitten`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body)
            })
                .then((response) => {
                    if (response.status >= 400 && response.status < 600) {
                        throw new Error(response.statusText);
                    }
                    return response;
                })
                .then(() => {
                    refreshData();
                    toast("New Kitten Created Successfully", { type: toast.TYPE.SUCCESS });

                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                })
                .catch((error) => {
                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                    toast(error.toString(), { type: toast.TYPE.ERROR });
                    console.error(error);
                    refreshData();
                });
        } else if (type?.value === "litter") {
            const body = { litterName, litterDescription };
            await fetch(`/api/litter`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body)
            })
                .then((response) => {
                    if (response.status >= 400 && response.status < 600) {
                        throw new Error(response.statusText);
                    }
                    return response;
                })
                .then(() => {
                    refreshData();
                    toast("New Litter Created Successfully", { type: toast.TYPE.SUCCESS });

                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                })
                .catch((error) => {
                    setTimeout(function () {
                        setLoading(false);
                    }, 200);
                    toast(error.toString(), { type: toast.TYPE.ERROR });
                    console.error(error);
                    refreshData();
                });
        }
    };

    let datapoints = data.kittens.find((x: KittenData) => x.id.toString() === selectedKitten?.value)?.datapoints

    return (
        <div className="shadow-lg card-body card bg-base-100 text-accent">
            <form onSubmit={submitData}>
                <div className="text-2xl font-bold text-primary card-title">New Data Entry</div>
                <div className="pt-6 font-light ">Type</div>
                <Select
                    onChange={(option: TypeOptions | null) => {
                        setType(option);
                        resetInput();
                    }}
                    defaultValue={typeOptions[0]}
                    placeholder="New Data Type"
                    options={typeOptions}
                    value={type}
                />
                <div className="divider"></div>
                {type !== null && type["value"] == "data" ? (
                    <>
                        <div className="font-light ">Kitten Name</div>
                        <Select
                            onChange={(e) => setSelectedKitten(e)}
                            placeholder="Kitten Name"
                            options={KittenOptions}
                            value={selectedKitten}
                        />
                        <div className="grid grid-cols-2 gap-4 pt-3 mx-auto">
                            <div>
                                <div className="font-light">Starting Weight</div>
                                <NumberPicker
                                    min={0}
                                    defaultValue={350}
                                    onChange={(e: number | null) => setStartWeight(e)}
                                    placeholder="0"
                                    precision={0}
                                //format={{ style: 'unit', unit: 'gram', unitDisplay: 'long' }}
                                />
                            </div>
                            <div>
                                <div className="font-light ">Final Weight</div>
                                <NumberPicker
                                    min={0}
                                    defaultValue={350}
                                    onChange={(e: number | null) => setFinalWeight(e)}
                                    placeholder="0"
                                    precision={0}
                                //format={{ style: 'unit', unit: 'gram', unitDisplay: 'long' }}
                                />
                            </div>
                        </div>
                        <div className="py-4">
                            <div className="font-light ">Time</div>

                            <Suspense fallback={<HeartsLoader></HeartsLoader>}>

                                <DateTimePicker
                                    renderInput={(props) => <TextField {...props} />}
                                    className="bg-white"
                                    value={selectedTime as Date | null}
                                    onChange={(value: any) => setSelectedTime(value ? value : null)}

                                />
                            </Suspense>
                        </div>
                    </>
                ) : null}

                {type !== null && type["value"] == "kitten" ? (
                    <>
                        <div className="font-light pt ">Kitten Name</div>
                        <input
                            className="w-full my-2 bg-white input-sm input input-bordered"
                            onChange={(e) => setKittenName(e.target.value)}
                            placeholder="Enter a name"
                            type="text"
                            value={kittenName}
                            id="name"
                        />
                        <div className="font-light ">Description</div>
                        <input
                            className="w-full my-2 bg-white input-sm input input-bordered"
                            onChange={(e) => setKittenDescription(e.target.value)}
                            placeholder="Enter a description"
                            type="text"
                            value={kittenDescription}
                            id="description"
                        />
                        <div className="font-light ">Kitten Litter</div>
                        <Select
                            className="pt-2"
                            onChange={(e: any) => setKittenLitter(e)}
                            defaultValue={LitterOptions[0]}
                            placeholder="Select a Litter"
                            options={LitterOptions}
                            value={kittenLitter}
                        />
                        <div className="py-4">
                            <div className="font-light ">Birthdate</div>
                            <Suspense fallback={<HeartsLoader />}>
                                <DateTimePicker
                                    renderInput={(props) => <TextField {...props} />}
                                    className="bg-white"
                                    value={kittenBirthday}
                                    onChange={(value: any) => setKittenBirthday(value)}
                                />
                            </Suspense>
                        </div>
                        <input
                            className="w-full my-2 bg-white input-sm input input-bordered"
                            onChange={(e) => setImage(e.target.value)}
                            placeholder="Image"
                            type="text"
                            value={image}
                        />
                        <input
                            className="pb-4 font-thin transition-colors bg-transparent hover:text-secondary"
                            type="button"
                            value="Upload an image?"
                            onClick={() => setUploaderVisible(true)}
                        />
                        {uploaderVisible ? (
                            <>
                                <div className="max-w-4xl py-2 m-auto">
                                    <ImageUpload changeImageUrl={changeImageUrl} />
                                </div>
                            </>
                        ) : null}
                    </>
                ) : null}

                {type !== null && type["value"] == "litter" ? (
                    <>
                        <div className="font-light pt ">Litter Name</div>
                        <input
                            className="w-full my-2 bg-white input-sm input input-bordered"
                            onChange={(e) => setLitterName(e.target.value)}
                            placeholder="Enter a name"
                            type="text"
                            value={litterName}
                            id="name"
                        />
                        <div className="font-light ">Description</div>
                        <input
                            className="w-full my-2 bg-white input-sm input input-bordered"
                            onChange={(e) => setLitterDescription(e.target.value)}
                            placeholder="Enter a description"
                            type="text"
                            value={litterDescription}
                            id="description"
                        />
                    </>
                ) : null}
                <div className="divider"></div>
                {loading ? (
                    <HeartsLoader />
                ) : (
                    <div className="btn-group">
                        <input
                            disabled={!selectedKitten && !kittenName && !litterName}
                            className="btn btn-primary"
                            type="submit"
                            value="Create"
                        />
                        <a className="btn btn-ghost btn-accent" href="#" onClick={() => router.push("/")}>
                            Cancel
                        </a>
                    </div>
                )}
            </form>
            {selectedKitten && typeof datapoints != "undefined" ? (
                <div className="pt-4">
                    <KittenDataTable
                        kittenData={
                            datapoints
                        }
                        refreshData={refreshData}></KittenDataTable>
                </div>
            ) : null}
        </div>
    )
}