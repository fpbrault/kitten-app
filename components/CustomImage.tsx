import Image from "next/image";
import Link from "next/link";

interface Props {
  src: string;
  alt: string;
  wider?: boolean;
  priority?: boolean;
}

export default function CustomImage({ src, alt, ...otherProps }: Props) {
  return (
    <>
      <Link href={src}>
        <Image
          className="w-12"
          alt={alt}
          src={src}
          layout="responsive"
          objectFit="scale-down"
          height={250}
          width={500}
          {...otherProps}
        />
      </Link>
    </>
  );
}
