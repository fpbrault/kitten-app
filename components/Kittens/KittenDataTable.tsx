import React from "react";
import MaterialTable from "@material-table/core";
import { ThemeProvider, createTheme } from "@mui/material";
import { TablePagination, TablePaginationProps } from "@mui/material";
import dateFormat from "dateformat";
function PatchedPagination(props: TablePaginationProps) {
  const { ActionsComponent, onPageChange, onRowsPerPageChange, ...tablePaginationProps } = props;

  return (
    <TablePagination
      {...tablePaginationProps}
      onPageChange={onPageChange}
      onRowsPerPageChange={onRowsPerPageChange}
      ActionsComponent={(subprops) => {
        const { onPageChange, ...actionsComponentProps } = subprops;
        return (
          // @ts-expect-error ActionsComponent is provided by material-table
          <ActionsComponent {...actionsComponentProps} onChangePage={onPageChange} />
        );
      }}
    />
  );
}

async function deleteKittenDataPost(id: number): Promise<void> {
  await fetch(`/api/kittendatapost/${id}`, {
    method: "DELETE"
  });
}

type Props = {
  kittenData: {
    id: number;
    startWeight: number;
    finalWeight: number;
    time: Date;
    kittenId: number;
  }[];
  refreshData: () => void;
};

type RowData = {
  id: number;
  startWeight: number;
  finalWeight: number;
  time: Date;
  kittenId: number;
};

const KittenDataTable: React.FC<Props> = (props) => {
  const defaultMaterialTheme = createTheme();
  return (
    <div style={{ maxWidth: "100%" }}>
      <ThemeProvider theme={defaultMaterialTheme}>
        <MaterialTable
          components={{
            Pagination: PatchedPagination
          }}
          columns={[
            {
              title: "Date",
              field: "time",
              defaultSort: "desc",
              cellStyle: {
                fontSize: "13px",
                textAlign: "center"
              },
              type: "datetime",
              render: function formatDate(row) {
                return <span>{dateFormat(row["time"], "dd mmm yy, h:MM TT")}</span>;
              }
            },
            { title: "id", field: "id", hidden: true },
            {
              title: "Start",
              field: "startWeight",
              type: "numeric",
              cellStyle: {
                fontSize: "12px",
                textAlign: "center"
              }
            },
            {
              title: "End",
              field: "finalWeight",
              type: "numeric",
              cellStyle: {
                fontSize: "12px",
                textAlign: "center"
              }
            }
          ]}
          data={props.kittenData}
          title="Data entries"
          actions={[
            {
              icon: "delete",
              tooltip: "Delete Entry",
              onClick: (rowData: RowData) => {
                if (confirm("Confirm delete?")) {
                  deleteKittenDataPost(rowData.id).then(() => {
                    setTimeout(function () {
                      props.refreshData();
                    }, 800);
                  });
                }
              }
            }
          ]}
          options={{
            search: false,
            actionsColumnIndex: -1,
            headerStyle: {
              backgroundColor: "#D8DBE2",
              color: "black",
              padding: "0px",
              fontSize: "12px",
              textAlign: "center"
            },
            rowStyle: {
              padding: 0,
              fontSize: "12px",
              textAlign: "center"
            }
          }}
        />
      </ThemeProvider>
    </div>
  );
};

export default KittenDataTable;
