import React from "react";
import ReactMarkdown from "react-markdown";
import Link from "next/link";
//import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { KittenProps } from "types/kittens";


const loaderProp = ({ src }: { src: string }) => {
  return src;
}

const Kitten: React.FC<{ kitten: KittenProps }> = ({ kitten }) => {
  //const { t } = useTranslation("common");
  //const litterName = kitten.litter ? kitten.litter.name : null;
  return (
    <Link href={"/kitten/" + kitten.id}>
      <div className="grid items-center justify-center max-w-xs grid-cols-2 mx-auto text-center transition-colors shadow-lg cursor-pointer card bg-base-100 hover:bg-secondary">
        {kitten.image ? (
          <Image height={300} width={300} src={kitten.image} unoptimized loader={loaderProp} className="object-cover w-40 h-40 rounded-lg" alt={kitten.name} />
        ) : (
          <Image height={300} width={300} src="/cat.png" className="object-cover w-40 h-40 p-6 rounded-lg bg-primary" alt={kitten.name} />
        )}
        <div>
          <div className="text-2xl">{kitten.name}</div>
          <ReactMarkdown>{kitten.content}</ReactMarkdown>
        </div>
      </div>
    </Link>
  );
};

export default Kitten;
