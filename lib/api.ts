import fs from 'fs'
import { join } from 'path'
import matter from 'gray-matter'
import { serialize } from "next-mdx-remote/serialize";
import { postsFilePaths, POSTS_PATH, pageFilePaths } from "./mdxUtils";
import path from "path";

const postsDirectory = join(process.cwd(), '_posts')

export function getPostSlugs() {
    return fs.readdirSync(postsDirectory)
}

export async function getPostBySlug({ params, locale }: any) {
    const realSlug = params.slug.replace(/\.mdx$/, "");

    const postFilePaths = path.join(POSTS_PATH, `${realSlug}.${locale}.mdx`);
    let filePath;
    if (fs.existsSync(postFilePaths)) {
        filePath = postFilePaths;
    } else if (path.join(POSTS_PATH, `${realSlug}.fr.mdx`)) {
        filePath = path.join(POSTS_PATH, `${realSlug}.fr.mdx`)
    }
    else {
        filePath = path.join(POSTS_PATH, `${realSlug}.en.mdx`);
    }
    const fileContents = fs.readFileSync(filePath);

    const { content, data } = matter(fileContents);

    const mdxSource = await serialize(content, {
        // Optionally pass remark/rehype plugins
        mdxOptions: {
            remarkPlugins: [],
            rehypePlugins: []
        },
        scope: data
    });
    return {
        props: {
            slug: realSlug,
            source: mdxSource,
            frontMatter: data
        }
    };
}
export async function getPostsPaths({ locales }: any) {
    const paths = postsFilePaths
        // Remove file extensions for page paths
        .map((path) => path.replace(/\.mdx?$/, ""))
        // Map the path into the static paths object required by Next.js
        .map((slug) =>
            locales.map((locale: string) => ({ params: { slug: slug.replace(/\.[^/.]+$/, "") }, locale }))
        )
        .flat();
    return {
        paths,
        fallback: false
    };
}

export async function getPagesPaths({ locales }: any) {

    const paths = pageFilePaths
        // Remove file extensions for page paths
        .map((path) => path.replace(/\.mdx?$/, ""))
        // Map the path into the static paths object required by Next.js
        .map((slug) =>
            locales.map((locale: string) => ({ params: { slug: slug.replace(/\.[^/.]+$/, "") }, locale }))
        )
        .flat();



    return {
        paths,
        fallback: false
    };
}

export async function getAllPosts({ locale, locales }: any) {

    const postPaths = await getPostsPaths({ locales })

    const posts = await Promise.all(postPaths.paths.filter((tag, index, array) => array.findIndex(t => t.locale == tag.locale && t.params.slug == tag.params.slug) == index).filter(path => path.locale == locale)
        .map(async (path) => await getPostBySlug({ params: path.params, locale: path.locale })
        ))

    // sort posts by date in descending order
    const sortedPosts = posts
        .sort((post1, post2) => ((post1).props.frontMatter.date > (post2).props.frontMatter.date ? -1 : 1)).filter(post => post.props.frontMatter.language == locale)
    return sortedPosts
}