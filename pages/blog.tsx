import React from "react";
import Layout from "components/Generic/Layout";
import Post from "components/Post";
import { getAllPosts } from "lib/api";
import { PostProps } from "types/kittens";

export const getStaticProps = async ({ locale, locales }: any) => {
  const posts = await getAllPosts({ locale, locales })
  return {
    props: { posts: posts }
  };
};



type Props = {
  posts: { props: PostProps }[]
}

const Blog: React.FC<Props> = (props) => {
  return (
    <Layout pageTitle="Les Petits Chatons - Blog">
      <div className="max-w-4xl pt-24 mx-auto bg-base-400"><div className="mx-4">

        {props.posts.map((post) => (
          <Post key={post?.props.frontMatter?.title} post={post.props} />
        ))}
      </div>
      </div>
    </Layout>
  );
};

export default Blog;
