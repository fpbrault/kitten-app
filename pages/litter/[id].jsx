import React from "react";
import ReactMarkdown from "react-markdown";
import Layout from "components/Generic/Layout";
import { prisma } from "lib/prisma";
import Kitten from "components/Kittens/Kitten";

export const getServerSideProps = async ({ params }) => {
  const litter = await prisma.litter.findUnique({
    where: {
      id: Number(params?.id) || -1
    },
    include: {
      Kitten: true
    }
  });
  return {
    props: litter
  };
};

const Litter = (props) => {


  return (
    <Layout pageTitle={"Les Petits Chatons - " + props.name}>
      <div className="flex flex-col justify-center p-4 pt-24 mt-4 align-center">
        <div className="mx-auto text-center">
          <div className="text-4xl font-bold">{props.name}</div>
          <div>
            <ReactMarkdown>{props.description}</ReactMarkdown>
          </div>
        </div>
        <main className="flex gap-4 mx-auto mt-10">
          {props.Kitten.map((kitten) => (
            <div key={kitten.id} className="kitten">
              <Kitten kitten={kitten} />
            </div>
          ))}
        </main>
      </div>
    </Layout>
  );
};

export default Litter;
