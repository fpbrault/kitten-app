import React from "react";
import Layout from "components/Generic/Layout";
import "react-widgets/styles.css";
import useSWR, { SWRConfig } from "swr";
//import { useSession } from "next-auth/react";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DataEntryForm from "components/Admin/DataEntryForm"

const fetcher = (url) => fetch(url).then((res) => res.json());

export const getServerSideProps = async () => {
  const data = await fetcher(process.env.NEXTAUTH_URL + "/api/kittendatapost");
  return {
    props: {
      fallback: {
        "/api/kittendatapost": data
      }
    }
  };
};

const Draft = () => {
  const { data, mutate } = useSWR("/api/kittendatapost", fetcher);
  const refreshData = () => {
    mutate("/api/kittendatapost");
  };
  //const { data: session } = useSession();

  return (
    <Layout pageTitle="Les Petits Chatons - Ajouter Données">
      <ToastContainer />
      <div className="max-w-4xl p-8 m-auto mt-24">
        <DataEntryForm data={data} refreshData={refreshData}></DataEntryForm>
      </div>
    </Layout>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Page = ({ fallback }) => {
  // SWR hooks inside the `SWRConfig` boundary will use those values.

  return (
    <SWRConfig value={{ fallback }}>
      { }
      <Draft
        data={{
          kittens: [],
          litters: []
        }}
        kittenDataPost={[]}
      />
    </SWRConfig>
  );
};

export default Page;
