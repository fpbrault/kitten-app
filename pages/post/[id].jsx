/* eslint-disable @next/next/no-img-element */
import React from "react";
import ReactMarkdown from "react-markdown";
import Layout from "components/Generic/Layout";
import Router from "next/router";
import { prisma } from "lib/prisma";
import { useSession } from "next-auth/react";
import Image from "next/image";

const loaderProp = ({ src }) => {
  return src;
}

export const getServerSideProps = async ({ params }) => {
  const post = await prisma.post.findUnique({
    where: {
      id: Number(params?.id) || -1
    },
    include: {
      author: {
        select: { name: true, image: true }
      },
      kitten: true
    }
  });
  return {
    props: post
  };
};

async function publishPost(id) {
  await fetch(`/api/publish/${id}`, {
    method: "PUT"
  });
  await Router.push("/");
}

async function deletePost(id) {
  await fetch(`/api/post/${id}`, {
    method: "DELETE"
  });
  await Router.push("/");
}

const Post = (props) => {
  const { data: session, status } = useSession();
  if (status === "loading") {
    return <div>Authenticating ...</div>;
  }
  const userHasValidSession = Boolean(session);
  const userIsAdmin = session?.user?.role === "admin";

  let title = props.title;
  if (!props.published) {
    title = `${title} (Draft)`;
  }

  return (
    <Layout pageTitle={"Les Petits Chatons - Blog - " + title}>
      <div className="flex flex-col items-center min-h-screen py-16 text-center bg-white rounded hover:shadow-lg">
        {props?.image ? (
          <Image
            width={1000}
            height={1000}
            className="object-cover max-w-full lg:max-w-4xl"
            src={props?.image}
            alt={props?.title}
            loader={loaderProp}
            priority
          />
        ) : null}
        <div className="pt-8 my-4 text-4xl font-bold">{title}</div>
        <div className="mb-2 text-lg font-thin">
          <div className="flex gap-2">
            {props?.author?.image ? (
              <Image
                width={32}
                height={32}
                src={props?.author?.image}
                className="object-cover w-8 h-8 rounded-full"
                alt={props?.author?.name.split(" ")[0]}
                loader={loaderProp}
              />
            ) : (
              <svg className="w-8 h-8" viewBox="0 0 24 24">
                <path
                  fill="currentColor"
                  d="M12,8L10.67,8.09C9.81,7.07 7.4,4.5 5,4.5C5,4.5 3.03,7.46 4.96,11.41C4.41,12.24 4.07,12.67 4,13.66L2.07,13.95L2.28,14.93L4.04,14.67L4.18,15.38L2.61,16.32L3.08,17.21L4.53,16.32C5.68,18.76 8.59,20 12,20C15.41,20 18.32,18.76 19.47,16.32L20.92,17.21L21.39,16.32L19.82,15.38L19.96,14.67L21.72,14.93L21.93,13.95L20,13.66C19.93,12.67 19.59,12.24 19.04,11.41C20.97,7.46 19,4.5 19,4.5C16.6,4.5 14.19,7.07 13.33,8.09L12,8M9,11A1,1 0 0,1 10,12A1,1 0 0,1 9,13A1,1 0 0,1 8,12A1,1 0 0,1 9,11M15,11A1,1 0 0,1 16,12A1,1 0 0,1 15,13A1,1 0 0,1 14,12A1,1 0 0,1 15,11M11,14H13L12.3,15.39C12.5,16.03 13.06,16.5 13.75,16.5A1.5,1.5 0 0,0 15.25,15H15.75A2,2 0 0,1 13.75,17C13,17 12.35,16.59 12,16V16H12C11.65,16.59 11,17 10.25,17A2,2 0 0,1 8.25,15H8.75A1.5,1.5 0 0,0 10.25,16.5C10.94,16.5 11.5,16.03 11.7,15.39L11,14Z"
                />
              </svg>
            )}
            {props?.author?.name.split(" ")[0] || "Les Petits Chatons"}
          </div>
        </div>

        <div className="prose-sm prose text-left sm:prose lg:prose-lg">
          <ReactMarkdown className="px-4">{props.content}</ReactMarkdown>
        </div>
        <div className="flex p-4">
          {!props.published && userHasValidSession && userIsAdmin && (
            <button className="mx-2 btn btn-secondary" onClick={() => publishPost(props.id)}>
              Publish
            </button>
          )}
          {userHasValidSession && userIsAdmin && (
            <button className="mx-2 btn btn-primary" onClick={() => deletePost(props.id)}>
              Delete
            </button>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default Post;
