import React from "react";
import { getProviders, signIn } from "next-auth/react";
import Link from "next/link";
import { ReactElement } from "react";
import { CommonProviderOptions } from "next-auth/providers";
import Layout from "components/Generic/Layout";

type Providers = {
  providers: CommonProviderOptions;
};

export default function SignIn({ providers }: Providers): ReactElement {
  return (<Layout pageTitle="Les Petits Chatons - Login">
    <div className="flex flex-col justify-center min-h-screen sm:py-12">
      <div className="p-10 mx-auto xs:p-0 md:w-full md:max-w-md">
        <div className="p-5">
          <div className="flex flex-col gap-2 justify-center">
            {Object.values(providers).map((provider) => (

              <button key={provider.name} type="button"
                className="transition btn hover:btn-accent btn-secondary btn-lg"
                onClick={() => signIn(provider.id)}>
                Sign in with {provider.name}
              </button>
            ))}
          </div>
        </div>
        <div className="py-5">
          <div className="grid grid-cols-2 gap-1">
            <div className="text-center sm:text-left whitespace-nowrap">
              <Link href="/">
                <button className="px-5 py-4 mx-5 text-sm font-normal text-black transition duration-200 rounded-lg cursor-pointer hover:bg-gainsboro focus:outline-none focus:bg-gainsboro">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    className="inline-block w-4 h-4 align-text-top">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M10 19l-7-7m0 0l7-7m-7 7h18"
                    />
                  </svg>
                  <span className="inline-block ml-1">Back to home page</span>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Layout >
  );
}

// This is the recommended way for Next.js 9.3 or newer
export async function getServerSideProps() {
  const providers = await getProviders();

  return {
    props: { providers }
  };
}

/*
// If older than Next.js 9.3
SignIn.getInitialProps = async () => {
  return {
    providers: await getProviders()
  }
}
*/
