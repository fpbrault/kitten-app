import React from "react";
import { GetServerSideProps } from "next";
import Layout from "components/Generic/Layout";
import { prisma } from "lib/prisma";
import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import useTranslation from "next-translate/useTranslation";
import HeartsLoader from "components/Generic/HeartsLoader";
import { KittenProps } from "types/kittens";

const Kitten = dynamic(() => import('components/Kittens/Kitten'), {
  suspense: true,
})

export const getServerSideProps: GetServerSideProps = async () => {
  const feed = await prisma.kitten.findMany({
    include: {
      litter: {
        select: {
          name: true
        }
      }
    },
    orderBy: { id: "desc" }
  });
  return {
    props: { feed }
  };
};

type Props = {
  feed: KittenProps[];
};

const Kittens: React.FC<Props> = (props) => {
  const { t } = useTranslation("common");
  return (
    <Layout pageTitle="Les Petits Chatons - Nos Chatons">
      <div className="w-full pt-32 mx-auto max-w-fit">
        <div className="mx-auto shadow stats-vertical lg:stats-horizontal stats">
          <div className="stat">
            <div className="stat-figure text-primary">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-8 h-8 stroke-current">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path>
              </svg>
            </div>
            <div className="stat-title">{t("kittens.stats.fostered")}</div>
            <div className="stat-value text-primary">
              {props.feed.filter((x) => x.birthdate.getFullYear() !== null).length}
            </div>
            {/* <div className="stat-desc">21% more than last month</div> */}
          </div>

          <div className="stat">
            <div className="stat-figure text-secondary">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-8 h-8 stroke-current">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M13 10V3L4 14h7v7l9-11h-7z"></path>
              </svg>
            </div>
            <div className="stat-title">{t("kittens.stats.litters")}</div>
            <div className="stat-value text-secondary">3</div>
          </div>
        </div>
      </div> <Suspense fallback={<HeartsLoader></HeartsLoader>}>
        <div className="grid max-w-2xl gap-4 pt-8 mx-auto mb-16 md:grid-cols-2">
          {props.feed.map((kitten) => (
            <Kitten key={kitten.id} kitten={kitten} />
          ))}
        </div></Suspense>
    </Layout>
  );
};

export default Kittens;
