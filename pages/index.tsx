import React from "react";
import Layout from "components/Generic/Layout";
import Link from "next/link";
import MiniPost from "components/MiniPost";
import useTranslation from "next-translate/useTranslation";
import { getAllPosts } from "lib/api";
import { PostProps } from "types/kittens";


export const getStaticProps = async ({ locale, locales }: any) => {
  const posts = await getAllPosts({ locale, locales })
  return {
    props: { posts: [posts[0]] }
  };
};



type Props = {
  posts: { props: PostProps }[]
}

const Blog: React.FC<Props> = (props) => {
  const { t } = useTranslation("common");
  return (
    <Layout pageTitle="Les Petits Chatons">
      <div className="min-h-screen hero lg:bg-base-200">
        <div className="flex flex-col-reverse items-center pb-32 text-center bg md:max-w-full lg:pb-0 lg:flex-row-reverse lg:text-left ">

          <div className="w-screen pt-16 mx-auto lg:pt-24 lg:max-w-sm">
            <h2 className="pb-8 text-2xl lg:hidden">{t("home.check-our-blog")}</h2>
            <div className="post indicator">
              <div className="indicator-item indicator-center badge badge-secondary">
                {t("home.new-blog-post")}
              </div>
              {props.posts.map((post) => (
                <div key={post?.props.frontMatter?.title} className="post">
                  <MiniPost post={post.props} />
                </div>
              ))}
            </div>
          </div>
          <section className="w-screen px-4 pt-48 pb-24 text-center lg:pt-24 lg:max-w-sm lg:pb-12 lg:bg-base-200 bg-base-300">
            <div >
              <h1 className="mb-5 text-4xl font-bold">Les Petits Chatons</h1>
              <p className="max-w-md mx-auto mb-5">{t("home.description")}</p>
              <div className="flex flex-wrap justify-center gap-2">
                {[
                  [t("home.about"), '/about'],
                  ['blog', '/blog'],
                  [t("home.our-kittens"), '/kittens'],
                ].map(([title, url]) => (
                  <Link key={title} href={url}>
                    <button className="btn btn-secondary">{title}</button>
                  </Link>
                ))}
              </div>
              <Link className="flex justify-center gap-2 mt-4" href="/donate">
                <button className="btn btn-accent">{t("home.donate")}</button>
              </Link>
            </div>
          </section>
        </div>
      </div>
    </Layout>
  );
};

export default Blog;
