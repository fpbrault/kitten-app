import React from "react";
import Layout from "components/Generic/Layout";
import Link from "next/link";
import { prisma } from "lib/prisma";
import { useSession } from "next-auth/react";
import KittenPost from "components/Kittens/KittenPost";
import dynamic from "next/dynamic";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { Suspense } from 'react'


const LineChart = dynamic(() => import("components/Kittens/LineChart"), { suspense: true });

const loaderProp = ({ src }) => {
  return src;
}

export const getServerSideProps = async ({ params }) => {
  const kitten = await prisma.kitten.findUnique({
    where: {
      id: Number(params?.id) || -1
    },
    include: {
      litter: {
        select: { name: true, id: true }
      },
      posts: {
        where: { published: true },
        orderBy: [
          {
            createdAt: "desc"
          }
        ]
      },
      datapoints: {}
    }
  });

  const idNumber = Number(params?.id);
  const datapoints = await prisma.$queryRaw`
    select date_trunc(\'day\', "time" AT TIME ZONE \'EST\') as "time" , count(id)::INTEGER, \
    round(avg("startWeight"))::INTEGER as "pre", round(avg("finalWeight"))::INTEGER as "post",\
    round(avg("finalWeight") - avg("startWeight")) as "delta", max("finalWeight" - "startWeight")::INTEGER as "delta" \
    from public."KittenDataPoint" WHERE "kittenId" = ${idNumber} group by 1, "kittenId" order by "kittenId", "time" asc`;
  return {
    props: { kitten, datapoints }
  };
};

const Kitten = (props) => {
  const { t } = useTranslation("common");
  const { data: session, status } = useSession();
  if (status === "loading") {
    return <div>Authenticating ...</div>;
  }
  const userHasValidSession = Boolean(session);
  const userIsAdmin = session?.user?.role === "admin";

  let data = {
    options: {
      chart: {
        id: "basic-line",
        group: "charts",
        zoom: {
          autoScaleYaxis: true
        },
        animateGradually: {
          enabled: false
        }
      },
      xaxis: {
        type: "datetime"
      },
      yaxis: [
        {
          title: {
            text: "Weight in grams"
          }
        }
      ],
      labels: {
        format: "dd/MM"
      },
      stroke: {
        width: [3, 3],
        stroke: {
          curve: "smooth"
        }
      },

      grid: {
        borderColor: "#e7e7e7",
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      tooltip: {
        shared: true,
        enabledOnSeries: [0, 1]
      },
      markers: {
        size: 5
      }
    },
    series: [
      {
        name: "Pre-feeding Average",
        data: props.datapoints.map((x) => ({ x: new Date(x.time), y: x.pre })),
        type: "line",
        dataLabels: {
          enabled: true,
          offsetY: -8,
          style: {
            fontSize: "8px"
          }
        }
      },
      {
        name: "Post-feeding Average",
        data: props.datapoints.map((x) => ({ x: new Date(x.time), y: x.post })),
        type: "line"
      } /* 
            {
                name: 'Ideal Weight(low)',
                data: lowRange,
                type: 'line'
            },
            {
                name: 'Ideal Weight(high)',
                data: highRange,
                type: 'line'
            } */
    ]
  };
  let data2 = {
    options: {
      chart: {
        id: "basic-bar",
        group: "charts",
        zoom: {
          autoScaleYaxis: true
        },
        animateGradually: {
          enabled: false
        }
      },
      xaxis: {
        type: "datetime"
      },
      labels: {
        format: "dd/MM"
      },
      stroke: {
        width: [2, 2]
      },
      dataLabels: {
        enabled: true,
        style: {
          fontSize: "8px"
        }
      },
      grid: {
        borderColor: "#e7e7e7",
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      markers: {
        size: 1
      }
    },
    series: [
      {
        name: "Delta",
        data: props.datapoints.map((x) => ({ x: new Date(x.time), y: x.delta })),
        type: "column"
      },
      {
        name: "Count",
        data: props.datapoints.map((x) => ({ x: new Date(x.time), y: x.count })),
        type: "column"
      }
    ]
  };

  return (
    <Layout pageTitle={"Les Petits Chatons - " + props.kitten.name}>
      <div className="z-20 flex flex-col justify-center w-full pt-20 bg-neutral text-neutral-content">
        <div className="pb-2 mx-auto text-6xl font-light">{props.kitten.name}</div>
        <div className="mx-auto avatar">
          <div className={props.kitten.image ? "w-32 h-32 rounded-full" : "w-32 h-32"}>
            {props.kitten.image ? (
              <Image loader={loaderProp}
                priority height={100} width={100} src={props.kitten.image} alt={props.kitten.name} />
            ) : (
              <Image
                priority height={100} width={100} src="/cat.png" alt={props.kitten.name} />
            )}
          </div>
        </div>
        {userHasValidSession && userIsAdmin ? (
          <div className="flex flex-row items-center justify-center my-2 ">
            <Link href={"/create"} className="mx-2 btn-primary btn-outline btn-sm btn">
              {t("kittens.add-post")}
            </Link>
            <Link href={"/kitten/edit/" + props.kitten.id} className="mx-2 btn-secondary btn-outline btn-sm btn">
              {t("kittens.edit")}
            </Link>
          </div>
        ) : null}
        <div className="grid content-center pb-4 text-center hover:grid-cols-1">
          <div className="text-base">
            {" "}
            {t("kittens.born") + ": " + props.kitten.birthdate.toDateString()}
          </div>
          <div className="text-base">{props?.kitten.content}</div>
        </div>
      </div>
      <Suspense fallback={'Loading...'}>
        <div className="flex justify-center pt-4 mx-2 bg-base-200">
          <div className="flex flex-col flex-wrap w-full mx-auto">
            <div className="w-full max-w-3xl mx-auto mb-4 text-center collapse rounded-box border-base-300 bg-base-content">
              <input type="checkbox" />
              <div className="text-xl font-medium max-w-1xl text-neutral-content collapse-title">
                {t("kittens.charts")}
              </div>
              <div className="collapse-content">
                <div className="grid w-full grid-cols-1 gap-2 mx-auto mb-4 text-center lg:grid-cols-1 max-w-7xl ">
                  <LineChart data={data} type="line"></LineChart>
                  <LineChart data={data2} type="line"></LineChart>
                </div>
              </div>
            </div>
            {props.kitten.posts.map((post) => (
              <KittenPost key={post.id} post={post} />
            ))}
          </div>
        </div>
      </Suspense>
    </Layout>
  );
};

export default Kitten;
