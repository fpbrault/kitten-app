import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import GithubProvider from "next-auth/providers/github";
import getConfig from "next/config";
/* import { PrismaAdapter } from "@next-auth/prisma-adapter";
import { prisma } from "lib/prisma";
import { AdapterUser } from "next-auth/adapters"; */

const { serverRuntimeConfig } = getConfig();

export default NextAuth({
  providers: [
    GoogleProvider({
      clientId: serverRuntimeConfig
        ? serverRuntimeConfig.GOOGLE_CLIENT_ID
        : process.env.GOOGLE_CLIENT_ID,
      clientSecret: serverRuntimeConfig
        ? serverRuntimeConfig.GOOGLE_CLIENT_SECRET
        : process.env.GOOGLE_CLIENT_SECRET,
      profile(profile) {
        return {
          id: profile.sub,
          name: profile.name,
          email: profile.email,
          image: profile.picture,
          role: profile.given_name
        };
      }
    }),
    GithubProvider({
      clientId: serverRuntimeConfig ? serverRuntimeConfig.GITHUB_ID : process.env.GITHUB_ID,
      clientSecret: serverRuntimeConfig ? serverRuntimeConfig.GITHUB_SECRET : process.env.GITHUB_ID
    })
  ],
  pages: {
    signIn: "/auth/signin"
  },
  callbacks: {
    signIn: async function ({ profile }) {
      if (

        profile?.email === "fpbrault@gmail.com" ||
        profile?.email === "fpbrault@coveo.com" ||
        profile?.email === "kathryn.grce@gmail.com"
      ) {
        return Promise.resolve(true);
      } else {
        return Promise.resolve(false);
      }
    },
    async session({ session, user, token }: { session: any, user: any, token: any }) {
      if (user?.role) {
        session.user.image = token.picture;
        session.user.role = typeof user.role == "string" ? user.role : "user";
      }
      return session;
    },
    async redirect({ baseUrl }) {
      return process.env.NEXTAUTH_URL ? process.env.NEXTAUTH_URL : baseUrl;
    }
  }
});
