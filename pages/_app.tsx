import { SessionProvider } from "next-auth/react";
import Script from 'next/script'

import React from "react";
import "styles/globals.css";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import ErrorBoundary from 'components/Generic/ErrorBoundary'
import { AppProps } from "next/app";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function App({ Component, pageProps }: AppProps) {


  return (
    <>
      <SessionProvider session={pageProps.session}>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <ErrorBoundary>
            <Component {...pageProps} />
          </ErrorBoundary>
        </LocalizationProvider>

      </SessionProvider>
      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-5J4XY84PFM"
        strategy="afterInteractive" />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-5J4XY84PFM');
        `}
      </Script></>
  );
}

export default App;
