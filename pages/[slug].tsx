import fs from "fs";
import matter from "gray-matter";
import { MDXRemote } from "next-mdx-remote";
import { serialize } from "next-mdx-remote/serialize";
import Head from "next/head";
import path from "path";
//import CustomLink from 'components/CustomLink';
import Layout from "components/Generic/Layout";
import { getPagesPaths } from "lib/api";
import { PAGES_PATH } from "lib/mdxUtils";

// Custom components/renderers to pass to MDX.
// Since the MDX files aren't loaded by webpack, they have no knowledge of how
// to handle import statements. Instead, you must include components in scope
// here.
const components = {
  //a: CustomLink,
  // It also works with dynamically-imported components, which is especially
  // useful for conditionally loading components for certain routes.
  // See the notes in README.md for more details.
  //TestComponent: dynamic(() => import('components/TestComponent')),
  Head
};

type Props = {
  source: any;
  frontMatter: any;
};

export default function PostPage({ source, frontMatter }: Props) {
  return (
    <>
      <Layout
        pageTitle={"Les Petits Chatons - " + frontMatter.title}
        fullpage={frontMatter?.fullpage}>
        <div
          className={"min-h-screen hero " + (!frontMatter.fullpage ? "bg-base-200" : "bg-primary")}>
          <div className="flex flex-col items-center py-16 mt-16 text-center lg:pb-16 lg:flex-row-reverse lg:text-left">
            <div className={!frontMatter.fullwidth ? "max-w-lg" : "w-screen"}>
              <div className="mb-5">
                <div
                  className={
                    !frontMatter.fullwidth
                      ? "prose-sm prose text-center sm:prose lg:prose-lg"
                      : "prose-sm prose text-center sm:prose lg:prose-lg overflow-auto w-full max-w-fit md:max-w-3xl xl:max-w-4xl mx-auto"
                  }>
                  <MDXRemote {...source} components={components} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}

export const getStaticProps = async ({ params, locale }: any) => {
  const realSlug = params.slug.replace(/\.mdx$/, "");
  const pageFilePaths = path.join(PAGES_PATH, `${realSlug}.${locale}.mdx`);
  let filePath;
  if (fs.existsSync(pageFilePaths)) {
    filePath = pageFilePaths;
  } else {
    filePath = path.join(PAGES_PATH, `${realSlug}.fr.mdx`);
  }
  const fileContents = fs.readFileSync(filePath);

  const { content, data } = matter(fileContents);

  const mdxSource = await serialize(content, {
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [],
      rehypePlugins: []
    },
    scope: data
  });
  return {
    props: {
      slug: realSlug,
      source: mdxSource,
      frontMatter: data
    }
  };
};

export const getStaticPaths = async ({ locales }: any) => {
  return getPagesPaths({ locales })
};
